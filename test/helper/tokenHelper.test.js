require('../test-setup');

const {expect} = require('chai');
const sinon = require('sinon');

const tokenHelper = require('../../src/helper/tokenHelper');

describe('TokenHelper', () => {
	describe('createUrlSaveToken', () => {
		it('should create a token without /, = or %', async () => {
			const token = await tokenHelper.createUrlSaveToken('someRandomToken@spec!al');
			
			expect(token).to.be.a('string');
			expect(token).to.not.contain('/');
			expect(token).to.not.contain('=');
		})
	})
	
	describe('validateUrlSaveToken', () => {
		it('should determine if a token is equal', async () => {
			const createdToken = await tokenHelper.createUrlSaveToken('someRandomToken@spec!al');
			const equal = await tokenHelper.validateUrlSaveToken(createdToken, 'someRandomToken@spec!al');
			const notEqual = await tokenHelper.validateUrlSaveToken(createdToken, 'notevenclose');
			
			expect(equal).to.be.true;
			expect(notEqual).to.false;
		})
	})
	
	describe('createAccessId', () => {
		it('should create a code', () => {
			expect(tokenHelper.createAccessId()).to.be.an('string');
			expect(tokenHelper.createAccessId().length).to.be.equal(8);
			expect(tokenHelper.createAccessId(5).length).to.be.equal(5);
		})
	})
})