require('../test-setup');

const {expect} = require('chai');
const sinon = require('sinon');

const urlHelper = require('../../src/helper/urlHelper');

describe('urlHelper', () => {
	describe('getBasePath', () => {
		it('should get the absolute base url', () => {
			const req = {host: 'localhost', protocol: 'http'};
			const baseUrl = urlHelper.getBasePath(req);
			
			expect(baseUrl).to.be.equal('http://localhost:3000');
		})
		it('should not add a default port', () => {
			process.env.PORT = 80;
			const req = {host: 'localhost', protocol: 'http'};
			const baseUrl = urlHelper.getBasePath(req);
			
			expect(baseUrl).to.be.equal('http://localhost');
			delete process.env.PORT;
		})
	})
	
	describe('getFullUrl', () => {
		it('should get the absolute url of the current path', () => {
			const req = {host: 'localhost', protocol: 'http', originalUrl: '/unit/test'};
			const fullUrl = urlHelper.getFullUrl(req);
			
			expect(fullUrl).to.be.equal('http://localhost:3000/unit/test');
		})
	})
});