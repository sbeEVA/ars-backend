require('../test-setup');

const {expect} = require('chai');
const sinon = require('sinon');

const mongoose = require('mongoose');
const config = require('config');

const User = require('../../src/model/user');
const AuthController = require('../../src/controller/auth');
const mailer = require('../../src/mail/connection');
const tokenHelper = require('../../src/helper/tokenHelper');

describe('AuthController', () => {
	before(async () => {
		
		//connection to test database
		await mongoose.connect(config.get('database_connection_string'));
		
		//create an unverified test user
		const userUnverified = new User({
			username: 'not-verified@email.de',
			email: 'not-verified@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'test',
			emailVerified: false,
			verifyToken: 'bm90LXZlcmlmaWVkQGVtYWlsLmRl',
			_id: '615605979e71990c3d9108ff'
		})
		await userUnverified.save();
		
		//create another unverified test user
		const userUnverified2 = new User({
			username: 'not-verifiedToo@email.de',
			email: 'not-verifiedToo@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'test',
			emailVerified: false,
			verifyToken: 'iamsus',
			_id: '615605979e71990c3d9108fe'
		})
		await userUnverified2.save();
		
		//create a test user
		const user = new User({
			username: 'email@email.de',
			email: 'email@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'test',
			emailVerified: true,
			verifyToken: 'ZW1haWxAZW1haWwuZGU',
			_id: '615f50e900392825bd894b3b'
		})
		await user.save();
	})
	
	after(async () => {
		//clean up
		await User.deleteMany({});
		await mongoose.disconnect();
	})
	
	describe('signup', () => {
		function prepareSignupRequestAndResponse() {
			const req = {
				body: {
					username: 'valid@email.com',
					email: 'valid@email.com',
					password: 'ValidPassword123!'
				}
			}
			
			const res = {
				statusCode: 500,
				message: null,
				status: function (code) {
					this.statusCode = code;
					return this;
				},
				json: function (data) {
					this.message = data.message;
					this.userId = data.userId;
				}
			}
			return {req, res};
		}
		
		it('can signup a user', async () => {
			const {req, res} = prepareSignupRequestAndResponse();
			
			sinon.stub(mailer, 'verify').resolves(true);
			sinon.stub(mailer, 'sendMail').resolves(true);
			sinon.stub(mailer, 'close').resolves(true);
			
			const response = await AuthController.signup(req, res, () => {
			});
			expect(response).to.be.undefined;
			expect(res.message).to.be.equal('User created.');
			expect(res.statusCode).to.be.equal(201);
			expect(res.userId).to.be.not.empty;
			expect(mailer.verify.calledOnce).to.be.true;
			expect(mailer.sendMail.calledOnce).to.be.true;
			expect(mailer.close.calledOnce).to.be.true;
			
			mailer.verify.restore();
			mailer.sendMail.restore();
			mailer.close.restore();
		})
		
		it('can signup a user with confirm url from frontend', async () => {
			const {req, res} = prepareSignupRequestAndResponse();
			
			req.body.confirmUrl = 'confirm-url-part';
			
			sinon.stub(mailer, 'verify').resolves(true);
			sinon.stub(mailer, 'sendMail').resolves(true);
			sinon.stub(mailer, 'close').resolves(true);
			
			const response = await AuthController.signup(req, res, () => {
			});
			expect(response).to.be.undefined;
			expect(res.message).to.be.equal('User created.');
			expect(res.statusCode).to.be.equal(201);
			expect(res.userId).to.be.not.empty;
			expect(mailer.verify.calledOnce).to.be.true;
			expect(mailer.sendMail.calledOnce).to.be.true;
			expect(mailer.close.calledOnce).to.be.true;
			
			mailer.verify.restore();
			mailer.sendMail.restore();
			mailer.close.restore();
		})
		
		it('throw error on invalid mail connection', async () => {
			const {req, res} = prepareSignupRequestAndResponse();
			
			sinon.stub(mailer, 'verify').rejects('not verified');
			sinon.stub(mailer, 'sendMail').resolves(true);
			sinon.stub(mailer, 'close').resolves(true);
			
			const response = await AuthController.signup(req, res, () => {
			});
			
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Could not send verification mail.');
			expect(res.statusCode).to.be.equal(500);
			expect(res).to.not.have.property('userid');
			expect(mailer.verify.calledOnce).to.be.true;
			expect(mailer.sendMail.called).to.be.false;
			expect(mailer.close.calledOnce).to.be.true;
			
			mailer.verify.restore();
			mailer.sendMail.restore();
			mailer.close.restore();
		})
		
		it('throw error on failing to send an email', async () => {
			const {req, res} = prepareSignupRequestAndResponse();
			
			sinon.stub(mailer, 'verify').resolves(true);
			sinon.stub(mailer, 'sendMail').rejects('not verified');
			sinon.stub(mailer, 'close').resolves(true);
			
			const response = await AuthController.signup(req, res, () => {
			});
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Could not send verification mail.');
			expect(res.statusCode).to.be.equal(500);
			expect(res).to.not.have.property('userid');
			expect(mailer.verify.calledOnce).to.be.true;
			expect(mailer.sendMail.calledOnce).to.be.true;
			expect(mailer.close.calledOnce).to.be.true;
			
			mailer.verify.restore();
			mailer.sendMail.restore();
			mailer.close.restore();
		})
	})
	
	describe('login', () => {
		it('throws an error on missing database access', async () => {
			sinon.stub(User, 'findOne');
			User.findOne.throws();
			
			const req = {
				body: {
					username: 'email@email.de',
					password: '123Abc!!',
				}
			}
			
			const result = await AuthController.login(req, {}, () => {
			});
			expect(result).to.be.an('error');
			expect(result).to.not.have.property('statusCode');
			
			User.findOne.restore();
		})
		
		it('throws an error on unknown email', async () => {
			const req = {
				body: {
					username: 'unknown@email.com',
					password: '123Abc!!'
				}
			}
			
			const result = await AuthController.login(req, {}, () => {
			});
			expect(result).to.be.an('error');
			expect(result).to.have.property('statusCode');
			expect(result.statusCode).to.be.equal(401);
			expect(result).to.have.property('message');
			expect(result.message).to.be.equal('User not found.');
		})
		
		it('throws an error on not verified email', async () => {
			const req = {
				body: {
					username: 'not-verified@email.de',
					password: '123Abc!!'
				}
			}
			const result = await AuthController.login(req, {}, () => {
			});
			expect(result).to.be.an('error');
			expect(result).to.have.property('statusCode');
			expect(result.statusCode).to.be.equal(401);
			expect(result).to.have.property('message');
			expect(result.message).to.be.equal('User not verified.');
		})
		
		it('throws an error on mismatching password', async () => {
			const req = {
				body: {
					username: 'email@email.de',
					password: 'Invalid123Abc!!',
				}
			}
			const result = await AuthController.login(req, {}, () => {
			});
			expect(result).to.be.an('error');
			expect(result).to.have.property('statusCode');
			expect(result.statusCode).to.be.equal(401);
			expect(result).to.have.property('message');
			expect(result.message).to.be.equal('Password incorrect.');
		})
		
		it('grant token on correct credentials', async () => {
			const req = {
				body: {
					username: 'email@email.de',
					password: '123Abc!!',
				}
			}
			const res = {
				statusCode: 500,
				token: null,
				userId: null,
				status: function (code) {
					this.statusCode = code;
					return this;
				},
				json: function (data) {
					this.token = data.token;
					this.userId = data.userId;
				}
			}
			
			await AuthController.login(req, res, () => {
			});
			expect(res).to.have.property('statusCode');
			expect(res.statusCode).to.be.equal(200);
			expect(res).to.have.property('token');
			expect(res.token).to.not.be.empty;
			expect(res).to.have.property('userId');
			expect(res.token).to.not.be.empty;
		})
	})
	
	describe('verifyEmail', () => {
		it('throws an error if token does not exists', async () => {
			const req = {params: {token: 'idonotexist'}};
			const res = {
				statusCode: 500,
				message: null,
				status: function (code) {
					this.statusCode = code;
					return this;
				},
				json: function (data) {
					this.message = data.message;
				}
			}
			
			const result = await AuthController.verifyEmail(req, res, () => {
			});
			expect(result).to.be.an('error');
			expect(result).to.have.property('statusCode');
			expect(result.statusCode).to.be.equal(401);
			expect(result).to.have.property('message');
			expect(result.message).to.be.equal('User not found.');
		})
		
		it('throws an error if email already verified', async () => {
			const req = {params: {token: 'ZW1haWxAZW1haWwuZGU'}};
			const res = {
				statusCode: 500,
				message: null,
				status: function (code) {
					this.statusCode = code;
					return this;
				},
				json: function (data) {
					this.message = data.message;
				}
			}
			
			const result = await AuthController.verifyEmail(req, res, () => {
			});
			expect(result).to.be.an('error');
			expect(result).to.have.property('statusCode');
			expect(result.statusCode).to.be.equal(409);
			expect(result).to.have.property('message');
			expect(result.message).to.be.equal('User is already verified.');
		})
		
		it('throws an error on malformed token', async () => {
			const req = {params: {token: 'iamsus'}};
			const res = {
				statusCode: 500,
				message: null,
				status: function (code) {
					this.statusCode = code;
					return this;
				},
				json: function (data) {
					this.message = data.message;
				}
			}
			
			const result = await AuthController.verifyEmail(req, res, () => {
			});
			expect(result).to.be.an('error');
			expect(result).to.have.property('statusCode');
			expect(result.statusCode).to.be.equal(409);
			expect(result).to.have.property('message');
			expect(result.message).to.be.equal('Validation token is unexpected.');
		})
		
		it('updates user to verified on valid token', async () => {
			const req = {params: {token: 'bm90LXZlcmlmaWVkQGVtYWlsLmRl'}};
			const res = {
				statusCode: 500,
				message: null,
				status: function (code) {
					this.statusCode = code;
					return this;
				},
				json: function (data) {
					this.message = data.message;
				}
			}
			
			await AuthController.verifyEmail(req, res, () => {
			});
			
			const updatedUser = await User.findOne({id: '615605979e71990c3d9108ff'});
			
			expect(res).to.have.property('statusCode');
			expect(res.statusCode).to.be.equal(200);
			expect(res).to.have.property('message');
			expect(res.message).to.not.be.empty;
			
			expect(updatedUser.emailVerified).to.be.true;
			expect(updatedUser.verifyToken).to.be.null;
		})
	})
})

