require('../test-setup');

const {expect} = require('chai');
const sinon = require('sinon');

const mongoose = require('mongoose');
const config = require('config');

const Session = require('../../src/model/session');
const User = require('../../src/model/user');
const Question = require('../../src/model/question');
const SessionQuestionController = require('../../src/controller/sessionQuestion');
const tokenHelper = require('../../src/helper/tokenHelper');

describe('SessionQuestionController', () => {
	before(async () => {
		
		//connection to test database
		await mongoose.connect(config.get('database_connection_string'));
		
		//create an test user
		const user = new User({
			username: 'someone@email.de',
			email: 'someone@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'test',
			emailVerified: true,
			verifyToken: null,
			sessions: ['617ad46e46a1abe1afab5785'],
			_id: '615605979e71990c3d9108ff'
		})
		await user.save();
		
		const session = new Session({
			name: 'Existing session',
			accessCode: 'YLMVJPLF',
			userId: '615605979e71990c3d9108ff',
			_id: '617ad46e46a1abe1afab5785'
		});
		await session.save();
		
		//create an different user
		const userOther = new User({
			username: 'else@email.de',
			email: 'else@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'Not you',
			emailVerified: true,
			verifyToken: null,
			sessions: ['617adb1308bc984c1713d0cc'],
			_id: '61a76a4a44e5c837a8ce03e1'
		})
		await userOther.save();
		
		const sessionElse = new Session({
			name: 'Existing session',
			accessCode: 'FLPJYLMV',
			userId: '61a76a4a44e5c837a8ce03e1',
			_id: '617adb1308bc984c1713d0cc'
		});
		await sessionElse.save();
	})
	
	after(async () => {
		//clean up
		await User.deleteMany({});
		await Session.deleteMany({});
		await Question.deleteMany({});
		await mongoose.disconnect();
	})
	
	describe('createQuestion', () => {
		it('throws on non-existing session');
		it('throws on foreign session');
		it('creates a question in database');
		
	})
	
	describe('editQuestion', () => {
		it('throws on non-existing session');
		it('throws on foreign session');
		it('throws on non-existing question');
		it('changes the question');
	})
	
	describe('getQuestions', () => {
		it('throws on non-existing session');
		it('throws on foreign session');
		it('returns empty array for session without questions');
		it('returns list of question for session with questions');
	})
	
	describe('getQuestion', () => {
		it('throws on non-existing session');
		it('throws on foreign session');
		it('throws on non-existing question');
		it('returns the question');
	})
	
	describe('deleteQuestion', () => {
		it('throws on non-existing session');
		it('throws on foreign session');
		it('throws on non-existing question');
		it('deletes the question from session');
	})
});