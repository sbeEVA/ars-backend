require('../test-setup');

const {expect} = require('chai');
const sinon = require('sinon');

const mongoose = require('mongoose');
const config = require('config');

const Session = require('../../src/model/session');
const User = require('../../src/model/user');
const Question = require("../../src/model/question");
const SessionController = require('../../src/controller/session');
const tokenHelper = require('../../src/helper/tokenHelper');
const AuthController = require("../../src/controller/auth");

describe('SessionController', () => {
	before(async () => {
		
		//connection to test database
		await mongoose.connect(config.get('database_connection_string'));
		
		//create an test user
		const user = new User({
			username: 'someone@email.de',
			email: 'someone@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'test',
			emailVerified: true,
			verifyToken: null,
			sessions: ['617ad46e46a1abe1afab5785'],
			_id: '615605979e71990c3d9108ff'
		})
		await user.save();
		
		const session = new Session({
			name: 'Existing session',
			accessCode: 'YLMVJPLF',
			userId: '615605979e71990c3d9108ff',
			_id: '617ad46e46a1abe1afab5785'
		});
		await session.save();
		
		const question = new Question({
			text: 'Question Text',
			type: 'SC',
			answers: [{text: '1', value: 0}, {text: '2', value: 1}],
		});
		
		const savedQuestion = await question.save();
		
		const session2 = new Session({
			name: 'Existing session with questions',
			accessCode: 'YLMVJPLG',
			userId: '615605979e71990c3d9108ff',
			_id: '61d82dac9701d328130bad36',
			questions: [{
				question: savedQuestion._id,
				isOpen: true,
				shareResults: false
			}]
		});
		
		await session2.save();
		
		//create an different user
		const userOther = new User({
			username: 'else@email.de',
			email: 'else@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'Not you',
			emailVerified: true,
			verifyToken: null,
			sessions: ['617adb1308bc984c1713d0cc'],
			_id: '61a76a4a44e5c837a8ce03e1'
		})
		await userOther.save();
		
		const sessionElse = new Session({
			name: 'Existing session',
			accessCode: 'FLPJYLMV',
			userId: '61a76a4a44e5c837a8ce03e1',
			_id: '617adb1308bc984c1713d0cc'
		});
		await sessionElse.save();
		
		//create an user without sessions
		const userOther2 = new User({
			username: 'else2@email.de',
			email: 'else2@email.de',
			password: await tokenHelper.createPasswordHash('123Abc!!'),
			displayName: 'Not you',
			emailVerified: true,
			verifyToken: null,
			_id: '617411db28f93c91bd23b2f7'
		})
		
		await userOther2.save();
	})
	
	after(async () => {
		//clean up
		await User.deleteMany({});
		await Session.deleteMany({});
		await Question.deleteMany({});
		await mongoose.disconnect();
	})
	
	function prepareCreateEditSessionRequestResponse() {
		const req = {
			body: {
				name: 'Name of Session',
			},
			userId: '615605979e71990c3d9108ff'
		}
		
		const res = {
			statusCode: 500,
			message: null,
			status: function (code) {
				this.statusCode = code;
				return this;
			},
			json: function (data) {
				this.message = data.message;
				this.sessionId = data.sessionId;
				this.session = data.session;
				this.sessions = data.sessions;
			}
		}
		return {req, res};
	}
	
	describe('createSession', () => {
		it('creates a session in database', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			
			const response = await SessionController.createSession(req, res, () => {
			});
			
			expect(response).to.be.undefined;
			expect(res.message).to.be.equal('Session created.');
			expect(res.statusCode).to.be.equal(201);
			expect(res.sessionId).to.be.not.empty;
		})
	})
	
	describe('editSession', () => {
		it('throws on non-existing session', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			req.params = {id: '617adb1308bc984c1713d0cb'};
			const response = await SessionController.editSession(req, res, () => {
			});
			
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Session not found.');
			expect(response.statusCode).to.be.equal(404);
		})
		
		it('throws on foreign sessions', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			req.params = {id: '617adb1308bc984c1713d0cc'};
			const response = await SessionController.editSession(req, res, () => {
			});
			
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Access to session not allowed.');
			expect(response.statusCode).to.be.equal(403);
		})
		
		it('can edit the name', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			req.params = {id: '617ad46e46a1abe1afab5785'};
			req.body.name = "New name";
			
			const response = await SessionController.editSession(req, res, () => {
			});
			
			expect(response).to.be.undefined;
			expect(res.statusCode).to.be.equal(200);
			
			const editedSession = await Session.findById(req.params.id);
			expect(editedSession).to.be.not.undefined;
			expect(editedSession.name).to.be.equal(req.body.name);
			expect(editedSession.isOpen).to.be.false;
		})
		
		it('can edit the open state', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			req.params = {id: '617ad46e46a1abe1afab5785'};
			req.body.open = true;
			
			const response = await SessionController.editSession(req, res, () => {
			});
			
			expect(response).to.be.undefined;
			expect(res.statusCode).to.be.equal(200);
			
			const editedSession = await Session.findById(req.params.id);
			expect(editedSession).to.be.not.undefined;
			expect(editedSession.isOpen).to.be.true;
		})
	})
	
	describe('getSession', () => {
		it('throws on non-existing session', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			req.params = {id: '617adb1308bc984c1713d0cb'};
			const response = await SessionController.getSession(req, res, () => {
			});
			
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Session not found.');
			expect(response.statusCode).to.be.equal(404);
		})
		
		it('throws on foreign sessions', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			req.params = {id: '617adb1308bc984c1713d0cc'};
			const response = await SessionController.getSession(req, res, () => {
			});
			
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Access to session not allowed.');
			expect(response.statusCode).to.be.equal(403);
		})
		
		it('returns existing session', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			req.params = {id: '617ad46e46a1abe1afab5785'};
			const response = await SessionController.getSession(req, res, () => {
			});
			
			expect(response).to.be.undefined;
			expect(res.statusCode).to.be.equal(200);
			expect(res.session).to.exist;
			expect(res.session.accessCode).to.be.equal('YLMVJPLF');
		})
	})
	
	describe('getSessionsOfAuthUser', () => {
		it('returns existing session of user', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			const response = await SessionController.getSessionsOfAuthUser(req, res, () => {
			});
			
			expect(response).to.be.undefined;
			expect(res.statusCode).to.be.equal(200);
			expect(res.sessions).to.exist;
			expect(res.sessions[0]).to.exist;
			expect(res.sessions[0].accessCode).to.be.equal('YLMVJPLF');
		})
		
		it('returns empty array on no sessions for user', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			req.userId = '61a76a4a44e5c837a8ce03e1';
			const response = await SessionController.getSessionsOfAuthUser(req, res, () => {
			});
			
			expect(response).to.be.undefined;
			expect(res.statusCode).to.be.equal(200);
			expect(res.sessions).to.exist;
			expect(res.sessions).to.be.an('array');
		})
	})
	
	describe('deleteSession', () => {
		it('throws on non-existing session', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			req.params = {id: '617adb1308bc984c1713d0cb'};
			const response = await SessionController.deleteSession(req, res, () => {
			});
			
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Session not found.');
			expect(response.statusCode).to.be.equal(404);
		})
		
		it('throws on foreign sessions', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			req.params = {id: '617adb1308bc984c1713d0cc'};
			const response = await SessionController.deleteSession(req, res, () => {
			});
			
			expect(response).to.an('error');
			expect(response.message).to.be.equal('Access to session not allowed.');
			expect(response.statusCode).to.be.equal(403);
		})
		
		it('removes session from database', async () => {
			const {req, res} = prepareCreateEditSessionRequestResponse();
			delete req.body;
			req.params = {id: '61d82dac9701d328130bad36'};
			
			const sessionBefore = await Session.findById(req.params.id);
			const questionId = sessionBefore.questions[0].question;
			expect(questionId).not.to.be.null;
			
			const response = await SessionController.deleteSession(req, res, () => {
			});
			
			expect(response).to.be.undefined;
			expect(res.statusCode).to.be.equal(204);
			
			const deletedSession = await Session.findById(req.params.id);
			expect(deletedSession).to.be.null;
			
			const deletedQuestion = await Question.findById(questionId);
			expect(deletedQuestion).to.be.null;
		})
		
		it('removes questions in session')
		
		it('removes results in session')
	})
})