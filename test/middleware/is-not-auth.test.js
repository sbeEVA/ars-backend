require('../test-setup');

const {expect} = require('chai');
const sinon = require('sinon');
const jwt = require('jsonwebtoken');

const notAuthMiddleware = require('../../src/middleware/is-not-auth');


describe('Not Auth middleware', () => {
	it('redirects if authenticated', () => {
		const req = {get: () => ('Bearer aTokenIWillAccept')};
		const res = {
			statusCode: 500,
			target: '',
			redirect: function (statusCode, target) {
				this.statusCode = statusCode;
				this.target = target;
			}
		}
		
		sinon.stub(jwt, 'verify');
		jwt.verify.returns({userId: 1});
		
		notAuthMiddleware(req, res, () => {
		});
		
		expect(res.statusCode).to.be.equal(403);
		expect(res.target).to.be.equal('back');
		
		jwt.verify.restore();
	})
	
	it('calls next on not authenticated', () => {
		const req = {
			get: () => {
			}
		};
		const res = {}
		const nextSpy = sinon.spy();
		
		notAuthMiddleware(req, res, nextSpy);
		
		expect(nextSpy.calledOnce).to.be.true;
	})
})