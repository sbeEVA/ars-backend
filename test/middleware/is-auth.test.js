require('../test-setup');

const {expect} = require('chai');
const sinon = require('sinon');
const jwt = require('jsonwebtoken');

const authMiddleware = require('../../src/middleware/is-auth');

describe('Auth middleware', () => {
	it('throws an error if the Authorization header is missing', () => {
		const req = {get: () => null};
		expect(authMiddleware.bind(this, req, {}, () => {
		})).to.throw('Not authenticated.');
	})
	
	it('throws an error if the Authorization header is in wrong format', () => {
		const req = {get: () => ('xyz')}
		expect(authMiddleware.bind(this, req, {}, () => {
		})).to.throw('Invalid token.');
	})
	
	it('throws an error if the Authorization token cannot be verified', () => {
		const req = {get: () => ('Bearer notARealToken')};
		expect(authMiddleware.bind(this, req, {}, () => {
		})).to.throw('Invalid token.');
	})
	
	it('yields a userId in request after decoding the token', () => {
		const req = {get: () => ('Bearer aTokenIWillAccept')};
		
		sinon.stub(jwt, 'verify');
		jwt.verify.returns({userId: 1});
		
		authMiddleware(req, {}, () => {
		});
		expect(req).to.have.property('userId', 1);
		expect(jwt.verify.called).to.be.true;
		
		jwt.verify.restore();
	})
})