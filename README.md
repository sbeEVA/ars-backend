# Audio Response System API

## Configuration

### Local system

To define paths on a local development system, see the `config`-Folder. In `config/unittest`, special settings for test
environment can be set (e.g. testing database).

Change at least these settings in the `default.json`

* database_connection_string: Set to a running MongoDB Instance. The collections will be set up by themselves, but you
  have to create the database.
* smtp_connection_string: Set to a connection of a accessible mail server. The mail server must allow your sender
  address and may reach the to-Address, so the e-mail-addresses of the user you want to log in with.
* smtp_send_address: This is the sender of the internal mails.

If these are set, just run `npm run install`, then `npm run start:dev` for nodemon, or `npm run start` for using `node`.

### Production

Many settings are done by setting an environment variable on the system. This will be prioritized over the
config-Settings. Settings, that's need to be done at the environment are:

* PORT: 3000 if not set
* SMTP_CONNECTION_STRING: Fallback to config if not set
* SMTP_SEND_ADDRESS: Fallback to config if not set
* MONGODB_URI: Fallback to config if not set
* FRONTEND_URL: URL to the Frontend Server
* NODE_ENV: Should be 'production' on live server
* JWT_SECRET: A secret unique hash to create a secret JWT

## Testing

Tests are placed in the `test` folder and called by `npm run test`.

Make sure you require the `test-setup.js` at the beginning of your test files.