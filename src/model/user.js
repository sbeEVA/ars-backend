/**
 * User model.
 * Builds the model and schema for a user object in the database.
 * @author SBE
 */

const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
		username: {
			type: String,
			required: true
		},
		password: {
			type: String,
			required: true
		},
		email: {
			type: String,
			required: true
		},
		displayName: {
			type: String,
			required: true
		},
		language: {
			type: String,
			required: true,
			default: 'de-DE'
		},
		emailVerified: {
			type: Boolean,
			required: true,
			default: false
		},
		verifyToken: {
			type: String,
			required: false
		}
	},
	{
		timestamps: true
	})

module.exports = mongoose.model('User', userSchema);