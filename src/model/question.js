/**
 * Session model.
 * Builds the model and schema for a session object in the database.
 * @author SBE
 */

const mongoose = require('mongoose');

const questionAnswerSchema = new mongoose.Schema({
	text: {
		type: String,
		required: true
	},
	value: {
		type: Number,
		required: true,
	},
})

const questionSchema = new mongoose.Schema({
		text: {
			type: String,
			required: true
		},
		type: {
			type: String,
			required: true
		},
		answers: [questionAnswerSchema]
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model('Question', questionSchema);