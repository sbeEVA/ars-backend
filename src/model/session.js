/**
 * Session model.
 * Builds the model and schema for a session object in the database.
 * @author SBE
 */

const mongoose = require('mongoose');

const sessionQuestionSchema = new mongoose.Schema({
	question: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Question',
		required: true
	},
	isOpen: {
		type: Boolean,
		required: true,
		default: false
	},
	shareResults: {
		type: Boolean,
		required: true,
		default: false
	},
	results: [{
		type: mongoose.Schema.Types.Mixed,
	}]
})

const sessionSchema = new mongoose.Schema({
		name: {
			type: String,
			required: true
		},
		accessCode: {
			type: String,
			required: true
		},
		isOpen: {
			type: Boolean,
			required: true,
			default: false
		},
		userId: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
			required: true
		},
		questions: [sessionQuestionSchema]
	},
	{
		timestamps: true
	})

module.exports = mongoose.model('Session', sessionSchema);