const url = require('url');

function getBasePath(req) {
	const urlParts = _getBaseUrlParts(req);
	
	return url.format(urlParts);
}

function getFullUrl(req) {
	const urlParts = _getBaseUrlParts(req);
	urlParts.pathname = req.originalUrl;
	
	return url.format(urlParts);
}

function _getBaseUrlParts(req) {
	const urlParts = {
		protocol: req.protocol,
		hostname: req.host
	};
	
	const port = process.env.PORT ?? 3000;
	if (![80, 443].includes(parseInt(port))) {
		urlParts.port = port;
	}
	return urlParts;
}

module.exports.getBasePath = getBasePath;
module.exports.getFullUrl = getFullUrl;