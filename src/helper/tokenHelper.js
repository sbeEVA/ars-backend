const {Buffer} = require('buffer');

const URLSafeBase64 = require('urlsafe-base64');
const bcrypt = require('bcrypt');

module.exports.createUrlSaveToken = async function (tokenContent) {
	const buffer = Buffer.from(tokenContent);
	return await URLSafeBase64.encode(buffer);
}

module.exports.validateUrlSaveToken = async function (token, expectedContent) {
	const decodedToken = await URLSafeBase64.decode(token);
	return decodedToken.toString() === expectedContent;
}

module.exports.createPasswordHash = async function (password) {
	return await bcrypt.hash(password, 10)
}

module.exports.validatePasswordHash = async function (hashedPassword, password) {
	return await bcrypt.compare(password, hashedPassword);
}

module.exports.createAccessId = function (length = 8) {
	let code = '';
	const characters = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
	for (let i = 0; i < length; i++) {
		code += characters.charAt(Math.floor(Math.random() * characters.length));
	}
	return code;
}