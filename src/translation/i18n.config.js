const i18n = require('i18n');
const path = require('path');

i18n.configure({
	locales: ['de-DE'],
	defaultLocale: 'de-DE',
	queryParameter: 'lang',
	directory: path.join('.', 'translation'),
	api: {
		'__': 'translate',
		'__n': 'translateN'
	},
});

module.exports.i18n = i18n;