const i18n = require('i18n');
const path = require("path");

i18n.configure({
	locales: ['de-DE'],
	defaultLocale: 'de-DE',
	queryParameter: 'lang',
	directory: __dirname,
});

/**
 * @link https://phrase.com/blog/posts/node-js-i18n-guide/
 */
class LocaleService {
	
	constructor(i18nProvider) {
		this.i18nProvider = i18nProvider;
		
	}
	
	translate(string, args = undefined) {
		return this.i18nProvider.__(string, args)
	}
}

module.exports.translator = new LocaleService(i18n);