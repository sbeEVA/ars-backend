//import node core modules
const path = require('path');
const fs = require('fs');

//import node packages
const express = require('express');
const bodyParser = require('body-parser');

const helmet = require('helmet'); //for security
// const morgan = require('morgan'); //for access.log
const expressOasGenerator = require('express-oas-generator'); // for API documentation

const databaseConnection = require('./database/connection');
const socketHandler = require('./socket/handler');
const config = require('config');

//import custom middlewares
const controlHeaderMiddleware = require('./middleware/control-headers');
const errorHandlerMiddleware = require('./middleware/error-handler');

//import routes
const authRoutes = require('./route/auth');
const userRoutes = require('./route/user');
const sessionRoutes = require('./route/session');
const participateRoutes = require('./route/participate');

const app = express();

expressOasGenerator.handleResponses(app, {
	predefinedSpec: (spec) => spec,
	specOutputPath: path.join(__dirname, 'doc', 'swagger.json'),
	writeIntervalMs: 60 * 1000,
	swaggerUiServePath: 'api-docs',
	mongooseModels: ['User', 'Session', 'Question'],
	alwaysServeDocs: true,
	tags: ['auth', 'session', 'user', 'question'],
	specOutputFileBehavior: expressOasGenerator.SPEC_OUTPUT_FILE_BEHAVIOR.RECREATE
});

app.use(helmet());
// app.use(morgan('combined', {stream: fs.createWriteStream(path.join(__dirname, '..', 'logs', 'access.log'), {flags: 'a'})}));
app.use(controlHeaderMiddleware);
app.use(bodyParser.json());

//add routes here
app.use('/auth', authRoutes);
app.use('/user', userRoutes);
app.use('/session', sessionRoutes);
app.use('/participate', participateRoutes);

app.use(errorHandlerMiddleware);

expressOasGenerator.handleRequests();

const port = process.env.port || process.env.PORT || 3000;
databaseConnection
	.then(() => {
		const server = app.listen(port);
		
		const io = require('./socket/socket').init(server);
		io.on('connection', socketHandler.onConnect);
	})
	.catch((err) => {
		console.error(err);
	})