const jwt = require("jsonwebtoken");
const config = require("config");

const Session = require('../model/session');
const socket = require("../socket/socket");

module.exports = {
	onConnect: (socket) => {
		const token = socket.handshake.query.token;
		
		let decodedToken;
		try {
			decodedToken = jwt.verify(token, process.env.JWT_SECRET || config.get('jwt_secret'));
		} catch (err) {
			socket.disconnect();
			return;
		}
		
		if (decodedToken.sessionId) {
			socket.join('session:' + decodedToken.sessionId);
		} else if (decodedToken.userId) {
			//join the user to each of its sessions
			Session.find({userId: decodedToken.userId}).then(sessions => {
				sessions.forEach((session) => {
					socket.join('session:' + session._id.toString());
				})
			})
		}
		console.debug('Client connected', decodedToken.sessionId, decodedToken.userId);
	}
}