/**
 * Error handler middleware.
 * This middleware catches all the errors that occur in the application and prints and returns it
 * @author SBE
 */

module.exports = (error, req, res, next) => {
	console.error(error);
	const result = res.status(error.statusCode || 500).json({message: error.message, data: error.data});
	next();
	return result;
}