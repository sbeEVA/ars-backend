/**
 * Is-Not-Authenticated middleware.
 * Add this middleware to a route if you want this route to not be accessible for a logged in user.
 * @author SBE
 */

const isAuthMiddleware = require('./is-auth');

module.exports = (req, res, next) => {
	try {
		isAuthMiddleware(req, res, () => {
		});
		
		//redirect back
		res.redirect(403, 'back')
		
	} catch (err) {
		//is-auth throws an error, so continue;
		next();
	}
}