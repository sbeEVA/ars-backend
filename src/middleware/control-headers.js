/**
 * Control Header Middleware.
 * This middleware takes care of the http header which are required for this application.
 * @author SBE
 */

const config = require('config');

module.exports = (req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', config.get('access_allow_origin'));
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
	next();
}