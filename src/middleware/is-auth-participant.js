/**
 * Is-Authenticated middleware.
 * Add this middleware to a route if you want this route to be protected by participants only.
 * @author SBE
 */


const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
	const authHeader = req.get('Authorization');
	throwIfNotAuth(authHeader);
	
	const token = authHeader.replace('Bearer ', ''); //only the token
	
	let decodedToken;
	try {
		decodedToken = jwt.verify(token, process.env.JWT_SECRET || config.get('jwt_secret'));
	} catch (err) {
		err = new Error('Invalid token.');
		err.statusCode = 401;
		throw err;
	}
	
	throwIfNotAuth(decodedToken);
	throwIfNotAuth('sessionId' in decodedToken);
	
	req.sessionId = decodedToken.sessionId;
	next();
}

const throwIfNotAuth = (condition) => {
	if (!condition) {
		const err = new Error('Not authenticated.');
		err.statusCode = 401;
		throw err;
	}
}