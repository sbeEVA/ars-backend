/**
 * Session-Routes
 */

const express = require('express');

const router = express.Router();
const questionRouter = require('./question');

const sessionController = require('../controller/session');
const sessionCreateValidator = require('../validator/sessionCreate');
const sessionEditValidator = require('../validator/sessionEdit');
const idValidator = require('../validator/idValidator');
const isAuth = require('../middleware/is-auth');

router.put('/', isAuth, sessionCreateValidator, sessionController.createSession);
router.get('/', isAuth, sessionController.getSessionsOfAuthUser);
router.get('/:id', isAuth, idValidator, sessionController.getSession);
router.patch('/:id', isAuth, idValidator, sessionEditValidator, sessionController.editSession);
router.delete('/:id', isAuth, idValidator, sessionController.deleteSession);
router.use('/:id/question', questionRouter);

module.exports = router;