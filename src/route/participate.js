/**
 * Participate-Routes
 */

const express = require('express');

const router = express.Router();

const participateController = require('../controller/participate');
const isAuthParticipant = require("../middleware/is-auth-participant");
const idValidator = require("../validator/idValidator");
const answerValidator = require('../validator/participateAnswer');

router.get('/:code/exist', participateController.doesCodeExist);
router.get('/:id/join', idValidator, participateController.joinSession);
router.get('/session', isAuthParticipant, participateController.getSession)
router.post('/:questionId', answerValidator, isAuthParticipant, participateController.postAnswer)

module.exports = router;