const express = require('express');

const router = express.Router();

const userController = require('../controller/user');
const isAuth = require('../middleware/is-auth');

router.get('/', isAuth, userController.getCurrentUser)

module.exports = router;