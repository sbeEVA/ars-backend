/**
 * Authentication-Routes
 */

const express = require('express');

const router = express.Router();

const authController = require('../controller/auth');
const signupValidator = require('../validator/signup');
const loginValidator = require('../validator/login');
const isNotAuth = require('../middleware/is-not-auth');

router.put('/signup', signupValidator, isNotAuth, authController.signup);
router.post('/login', loginValidator, isNotAuth, authController.login);
router.get('/verify/:token', isNotAuth, authController.verifyEmail);

module.exports = router;