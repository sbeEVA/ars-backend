/**
 * Question-Routes
 */

const express = require('express');

const router = express.Router({mergeParams: true});

const questionController = require('../controller/sessionQuestion');
const idValidator = require('../validator/idValidator');
const questionCreateValidator = require('../validator/questionCreate');
const isAuth = require('../middleware/is-auth');

router.get('/', isAuth, idValidator, questionController.getQuestions);
router.put('/', isAuth, questionCreateValidator, questionController.createQuestion);
router.get('/:questionId', isAuth, idValidator, questionController.getQuestion);
router.patch('/:questionId', isAuth, idValidator, questionController.editQuestion);
router.delete('/:questionId', isAuth, idValidator, questionController.deleteQuestion);

module.exports = router;