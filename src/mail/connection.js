const nodemailer = require("nodemailer");
const config = require("config");

module.exports = nodemailer.createTransport(process.env.SMTP_CONNECTION_STRING || config.get('smtp_connection_string'));