const Session = require("../model/session");
const jwt = require("jsonwebtoken");
const config = require("config");
const socket = require("../socket/socket");
const {validationResult} = require("express-validator");

/**
 * Returns true or false if the code exists.
 * @param req 	params {code => access code of the session}
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.doesCodeExist = async (req, res, next) => {
	try {
		const session = await Session.findOne({accessCode: req.params.code.toUpperCase()});
		throwOnNoSession(session);
		throwOnClosedSession(session);
		
		res.status(200).json({open: true, sessionId: session._id});
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.joinSession = async (req, res, next) => {
	try {
		const session = await Session.findById(req.params.id);
		throwOnNoSession(session);
		throwOnClosedSession(session);
		
		const token = jwt.sign({
				participant: true,
				sessionId: session._id.toString()
			},
			process.env.JWT_SECRET || config.get('jwt_secret'),
			{expiresIn: '1d'}
		);
		
		res.status(200).json({token: token, sessionId: session._id.toString()})
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.getSession = async (req, res, next) => {
	try {
		const session = await Session.findById(req.sessionId).populate('questions.question').populate('questions.results');
		throwOnNoSession(session);
		
		res.status(200).json({session: session});
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.postAnswer = async (req, res, next) => {
	try {
		throwOnValidationError(req);
		
		const session = await Session.findById(req.sessionId).populate('questions.question').populate('questions.results');
		throwOnNoSession(session);
		throwOnClosedSession(session);
		
		const sessionQuestion = session.questions.id(req.params.questionId);
		throwOnNoQuestion(sessionQuestion);
		throwOnClosedQuestion(sessionQuestion);
		
		sessionQuestion.results.push(req.body.answer);
		
		await session.save();
		
		_emitChangeEvent(session);
		
		res.status(200).json({message: 'Answer added'});
	} catch (err) {
		next(err);
		return err;
	}
}

const _emitChangeEvent = (session) => {
	const room = socket.getIO().to('session:' + session._id.toString());
	if (room) {
		room.emit('session', {
			action: 'changed',
			session: session,
		})
	}
}

//region ErrorHandling
const throwOnValidationError = (req) => {
	const errors = validationResult(req).formatWith(({location, msg, param, value, nestedErrors}) => {
		return `${location}[${param}]: ${msg}`;
	});
	
	if (!errors.isEmpty()) {
		const err = new Error('Answer validation failed.');
		err.statusCode = 422;
		err.data = errors.array();
		throw err;
	}
}

const throwOnNoSession = (session) => {
	if (!session) {
		const err = new Error('Session not found.');
		err.statusCode = 404;
		throw err;
	}
}

const throwOnClosedSession = (session) => {
	if (!session.isOpen) {
		const err = new Error('Session is closed.');
		err.statusCode = 409; //state conflict
		throw err;
	}
}

const throwOnNoQuestion = (question) => {
	if (!question) {
		const err = new Error('Question not found.');
		err.statusCode = 404;
		throw err;
	}
}

const throwOnClosedQuestion = (question) => {
	if (!question.isOpen) {
		const err = new Error('Question is closed.');
		err.statusCode = 409;
		throw err;
	}
}

//endregion