/**
 * AuthController.
 * Handles the Authentication of a user.
 * @author SBE
 */
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('config');

const User = require('../model/user');
const mailer = require('../mail/connection');
const urlHelper = require('../helper/urlHelper');
const tokenHelper = require('../helper/tokenHelper');

const {translator} = require('../translation/localService');

/**
 * Signup-Action. Register a user.
 * The request must be validated by validator/signup.js
 * @param req
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.signup = async (req, res, next) => {
	try {
		throwOnValidationError(req); //validation must be done before
		
		const user = new User({
			username: req.body.username.toLowerCase(),
			email: req.body.email,
			password: await tokenHelper.createPasswordHash(req.body.password),
			displayName: req.body.displayName ?? req.body.username,
			emailVerified: false,
			verifyToken: await tokenHelper.createUrlSaveToken(req.body.email),
			language: req.body.locale ?? 'de-DE'
		})
		
		let verifyUrl;
		if (req.body.confirmUrl) {
			const frontendUrl = process.env.FRONTEND_URL || config.get('frontend_url');
			verifyUrl = frontendUrl + '/' + req.body.confirmUrl + '/' + user.verifyToken;
		} else {
			verifyUrl = urlHelper.getBasePath(req) + '/auth/verify/' + user.verifyToken;
		}
		await sendVerifyMail(user.email, user.displayName, verifyUrl, user.language);
		const savedUser = await user.save();
		
		res.status(201).json({
			message: "User created.",
			userId: savedUser._id.toString()
		})
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.verifyEmail = async (req, res, next) => {
	try {
		let loadedUser = await User.findOne({verifyToken: req.params.token});
		throwOnNoUser(loadedUser);
		throwOnVerifiedEmail(loadedUser);
		await throwOnTokenUserMismatch(loadedUser);
		
		loadedUser.emailVerified = true;
		loadedUser.verifyToken = null;
		const savedUser = await loadedUser.save();
		
		res.status(200).json({
			message: 'Token verified',
		});
	} catch (err) {
		next(err);
		return err;
	}
	
}


/**
 * Login-Action: Login an existing user and returns a JWT.
 * @param req
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.login = async (req, res, next) => {
	try {
		let loadedUser = await User.findOne({username: req.body.username.toLowerCase()});
		throwOnNoUser(loadedUser);
		throwOnUnverifiedEmail(loadedUser);
		throwOnMismatchPassword(await tokenHelper.validatePasswordHash(loadedUser.password, req.body.password));
		
		//user exists and password is equal, create a token
		let expiresIn = '1d';
		if (req.body.keepLoggedIn === true) {
			expiresIn = '100d';
		}
		const token = jwt.sign({
				username: loadedUser.username,
				userId: loadedUser._id.toString()
			},
			process.env.JWT_SECRET || config.get('jwt_secret'),
			{expiresIn: expiresIn}
		);
		
		res.status(200).json({token: token, userId: loadedUser._id.toString()})
	} catch (err) {
		next(err);
		return err;
	}
}


const sendVerifyMail = async (recipient, name, verifyUrl, locale) => {
	return mailer.verify()
		.then(() => {
			const message = {
				from: process.env.SMTP_SEND_ADDRESS || config.get('smtp_send_address'),
				to: recipient,
				subject: translator.translate('email_confirm_subject'),
				text: translator.translate("email_confirm_text_plain", {name: name, verifyUrl: verifyUrl}),
				html: translator.translate("email_confirm_text_html", {name: name, verifyUrl: verifyUrl})
			};
			
			return mailer.sendMail(message);
		})
		.then((result) => {
			mailer.close();
			return result;
		})
		.catch(err => {
			mailer.close();
			console.error(err);
			throw new Error('Could not send verification mail.');
		});
}

//region ErrorHandling
const throwOnValidationError = (req) => {
	const errors = validationResult(req).formatWith(({location, msg, param, value, nestedErrors}) => {
		return `${location}[${param}]: ${msg}`;
	});
	
	if (!errors.isEmpty()) {
		if (errors.array().includes('body[username]: username already exists.')) {
			const err = new Error('Username already exists.');
			err.statusCode = 409;
			throw err;
		}
		
		const err = new Error('Sign-up validation failed.');
		err.statusCode = 422;
		err.data = errors.array();
		throw err;
	}
}

const throwOnNoUser = (user) => {
	if (!user) {
		const err = new Error('User not found.');
		err.statusCode = 401;
		throw err;
	}
}

function throwOnUnverifiedEmail(user) {
	if (!user.emailVerified) {
		const err = new Error('User not verified.');
		err.statusCode = 401;
		throw err;
	}
}

function throwOnVerifiedEmail(user) {
	if (user.emailVerified) {
		const err = new Error('User is already verified.');
		err.statusCode = 409;
		throw err;
	}
}

async function throwOnTokenUserMismatch(user) {
	if (!await tokenHelper.validateUrlSaveToken(user.verifyToken, user.email)) {
		const err = new Error('Validation token is unexpected.');
		err.statusCode = 409;
		throw err;
	}
}

const throwOnMismatchPassword = (isEqual) => {
	if (!isEqual) {
		const err = new Error('Password incorrect.');
		err.statusCode = 401;
		throw err;
	}
}
//endregion ErrorHandling