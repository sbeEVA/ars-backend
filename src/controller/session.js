/**
 * SessionController.
 * Handles the Sessions of a user.
 * @author SBE
 */
const {validationResult} = require('express-validator');

const tokenHelper = require('../helper/tokenHelper');
const Session = require('../model/session');
const Question = require('../model/question');
const socket = require('../socket/socket');

/**
 * Returns a session by a parameter id, which is set in req.params
 * @param req 	params {id => Internal Session Id}
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.getSession = async (req, res, next) => {
	try {
		const session = await Session.findById(req.params.id).populate('questions.question').populate('questions.results');
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		res.status(200).json({session: session});
	} catch (err) {
		next(err);
		return err;
	}
}

/**
 * Returns all sessions of the currently logged in user
 * @param req
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.getSessionsOfAuthUser = async (req, res, next) => {
	try {
		const sessions = await Session.find({userId: req.userId}).populate('questions.question').populate('questions.results');
		
		res.status(200).json({sessions: sessions});
	} catch (err) {
		next(err);
		return err;
	}
}

/**
 * Creates a session. Parameter are given by post
 * @param req 	post {}
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.createSession = async (req, res, next) => {
	try {
		throwOnValidationError(req);
		
		const session = new Session({
			name: req.body.name,
			isOpen: req.body.open ?? false,
			userId: req.userId,
			accessCode: await _createUniqueAccessCode(),
		});
		
		const savedSession = await session.save();
		
		res.status(201).json({
			message: 'Session created.',
			sessionId: savedSession._id.toString()
		});
	} catch (err) {
		next(err);
		return err;
	}
}

/**
 * Edits a session by a parameter id, which is set in req.params. Changed values are defined in post
 * @param req 	params {id => Internal Session Id}
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.editSession = async (req, res, next) => {
	try {
		throwOnValidationError(req);
		
		const session = await Session.findById(req.params.id).populate('questions.question').populate('questions.results');
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		const closeSession = session.isOpen && !req.body.open;
		
		session.name = req.body.name ?? session.name;
		session.isOpen = req.body.open ?? session.isOpen;
		await session.save();
		
		if (closeSession) {
			_emitCloseEvent(session._id);
		} else {
			_emitChangeEvent(session);
		}
		
		res.status(200).json({
			session: session
		});
	} catch (err) {
		next(err);
		return err;
	}
}

/**
 * Deletes a session by a parameter id, which is set in req.params.
 * @param req 	params {id => Internal Session Id}
 * @param res
 * @param next
 * @return {Promise<*>}
 */
module.exports.deleteSession = async (req, res, next) => {
	try {
		const session = await Session.findById(req.params.id);
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		//delete questions
		const questionIds = session.questions.map(question => question.question);
		const result = await Question.deleteMany({_id: {$in: questionIds}});
		
		await Session.deleteOne({_id: req.params.id});
		
		_emitCloseEvent(session._id);
		
		res.status(204).json({
			message: 'Session deleted.'
		});
	} catch (err) {
		next(err);
		return err;
	}
}

//region Methods
/**
 * Creates a access code and takes care that it does not exist yet.
 * @returns {Promise<string>}
 * @private
 */
async function _createUniqueAccessCode() {
	let accessCode = null;
	while (true) {
		accessCode = tokenHelper.createAccessId();
		const existingSession = await Session.findOne({accessCode: accessCode});
		if (!existingSession) {
			break;
		}
	}
	return accessCode;
}

//endregion

const _emitChangeEvent = (session) => {
	try {
		const room = socket.getIO().to('session:' + session._id.toString());
		if (room) {
			room.emit('session', {
				action: 'changed',
				session: session,
			})
		}
	} catch (err) {
		console.error(err);
	}
}

const _emitCloseEvent = (sessionId) => {
	try {
		const room = socket.getIO().to('session:' + sessionId);
		if (room) {
			room.emit('session', {
				action: 'closed',
				sessionId: sessionId,
			})
		}
	} catch (err) {
		console.error(err);
	}
}

//region ErrorHandling
const throwOnValidationError = (req) => {
	const errors = validationResult(req).formatWith(({location, msg, param, value, nestedErrors}) => {
		return `${location}[${param}]: ${msg}`;
	});
	
	if (!errors.isEmpty()) {
		const err = new Error('Session validation failed.');
		err.statusCode = 422;
		err.data = errors.array();
		throw err;
	}
}

const throwOnNoSession = (session) => {
	if (!session) {
		const err = new Error('Session not found.');
		err.statusCode = 404;
		throw err;
	}
}

const throwOnForeignSession = (session, authUserId) => {
	if (session.userId?.toString() !== authUserId) {
		const err = new Error('Access to session not allowed.');
		err.statusCode = 403;
		throw err;
	}
}
//region ErrorHandling