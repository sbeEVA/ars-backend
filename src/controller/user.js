const User = require("../model/user");

/**
 * returns
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
module.exports.getCurrentUser = async (req, res, next) => {
	try {
		let loadedUser = await User.findById(req.userId);
		throwOnNoUser(loadedUser);
		
		const publicData = {
			username: loadedUser.username,
			displayName: loadedUser.displayName,
			email: loadedUser.email,
			createdAt: loadedUser.createdAt,
			updatedAt: loadedUser.updatedAt,
			language: loadedUser.language
		}
		
		res.status(200).json({user: publicData})
	} catch (err) {
		next(err);
		return err;
	}
}

const throwOnNoUser = (user) => {
	if (!user) {
		const err = new Error('User not found.');
		err.statusCode = 401;
		throw err;
	}
}