/**
 * QuestionController.
 * Handles the questions in a session of a user.
 * @author SBE
 */
const {validationResult} = require('express-validator');

const Question = require('../model/question');
const Session = require("../model/session");
const socket = require("../socket/socket");


/**
 * Returns all questions of a session
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
module.exports.getQuestions = async (req, res, next) => {
	try {
		const session = await Session.findById(req.params.id).populate('questions.question').populate('questions.results');
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		res.status(200).json({sessionQuestions: session.questions});
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.getQuestion = async (req, res, next) => {
	try {
		const session = await Session.findById(req.params.id).populate('questions.question').populate('questions.results');
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		const question = session.questions.id(req.params.questionId);
		throwOnNoQuestion(question);
		
		res.status(200).json({sessionQuestion: question});
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.createQuestion = async (req, res, next) => {
	try {
		throwOnValidationError(req);
		
		const session = await Session.findById(req.params.id);
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		const answers = [];
		if (req.body.answers && req.body.answers.length > 0) {
			req.body.answers.forEach((answer, index) => {
				const newAnswer = {text: answer.text, value: index};
				answers.push(newAnswer);
			})
		}
		
		const question = new Question({
			text: req.body.text,
			type: req.body.type,
			answers: answers,
		});
		
		const savedQuestion = await question.save();
		
		session.questions.push({
			question: savedQuestion._id,
			isOpen: req.body.open,
			shareResults: req.body.shareResults
		});
		await session.save();
		
		_emitChangeEvent(session);
		
		res.status(201).json({
			message: 'Question created.',
			questionId: session.questions[session.questions.length - 1]._id.toString()
		});
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.editQuestion = async (req, res, next) => {
	try {
		throwOnValidationError(req);
		
		const session = await Session.findById(req.params.id).populate('questions.question').populate('questions.results');
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		const sessionQuestion = session.questions.id(req.params.questionId);
		throwOnNoQuestion(sessionQuestion);
		const question = sessionQuestion.question;
		
		if (req.body.answers && req.body.answers.length > 0) {
			const answers = [];
			req.body.answers.forEach((answer, index) => {
				const editedAnswer = {text: answer.text, value: index};
				if (answer._id !== null) {
					const exists = sessionQuestion.question.answers.id(answer._id);
					if (exists) {
						editedAnswer._id = answer._id;
					}
				}
				answers.push(editedAnswer);
			});
			question.answers = answers;
		}
		question.text = req.body.text ?? question.text;
		question.type = req.body.type ?? question.type;
		await question.save();
		
		sessionQuestion.isOpen = req.body.open ?? sessionQuestion.isOpen;
		sessionQuestion.shareResults = req.body.shareResults ?? sessionQuestion.shareResults;
		
		await session.save();
		_emitChangeEvent(session);
		
		res.status(200).json({
			message: 'Question edited.'
		});
		
	} catch (err) {
		next(err);
		return err;
	}
}

module.exports.deleteQuestion = async (req, res, next) => {
	try {
		const session = await Session.findById(req.params.id);
		throwOnNoSession(session);
		throwOnForeignSession(session, req.userId);
		
		const sessionQuestion = session.questions.id(req.params.questionId);
		if (sessionQuestion) {
			session.questions.splice(session.questions.indexOf(sessionQuestion), 1);
			await session.save();
			
			//delete question object as well
			await Question.deleteOne({_id: sessionQuestion.question});
		} else {
			throwOnNoQuestion(false);
		}
		
		const newSession = await Session.findById(req.params.id).populate('questions.question').populate('questions.results');
		_emitChangeEvent(newSession);
		
		res.status(204).json({
			message: 'Question deleted.'
		});
	} catch (err) {
		next(err);
		return err;
	}
}

const _emitChangeEvent = (session) => {
	try {
		const room = socket.getIO().to('session:' + session._id.toString());
		if (room) {
			room.emit('session', {
				action: 'changed',
				session: session,
			})
		}
	} catch (err) {
		console.error(err);
	}
}

//region ErrorHandling
const throwOnValidationError = (req) => {
	const errors = validationResult(req).formatWith(({location, msg, param, value, nestedErrors}) => {
		return `${location}[${param}]: ${msg}`;
	});
	
	if (!errors.isEmpty()) {
		const err = new Error('Question validation failed.');
		err.statusCode = 422;
		err.data = errors.array();
		throw err;
	}
}

const throwOnNoQuestion = (question) => {
	if (!question) {
		const err = new Error('Question not found.');
		err.statusCode = 404;
		throw err;
	}
}

const throwOnNoSession = (session) => {
	if (!session) {
		const err = new Error('Session not found.');
		err.statusCode = 404;
		throw err;
	}
}

const throwOnForeignSession = (session, authUserId) => {
	if (session.userId?.toString() !== authUserId) {
		const err = new Error('Access to session not allowed.');
		err.statusCode = 403;
		throw err;
	}
}

//endregion