const {body} = require("express-validator");

module.exports = [
	body('username').isEmail().withMessage('username required.'),
	body('password').not().isEmpty().withMessage('password required.'),
	body('keepLoggedIn').optional().isBoolean()
]