const {body} = require('express-validator');

const User = require('../model/user');

module.exports = [
	body('email').isEmail().withMessage('email required.'),
	body('username').isEmail().withMessage('username required.')
		.custom((value, {req}) => {
			return User.findOne({username: value}).then(existingUser => {
				if (existingUser) {
					return Promise.reject('username already exists.');
				}
			})
		})
		.normalizeEmail(),
	body('password').trim().matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
		.withMessage('Password guideline not fulfilled (Min. 8 characters with one upper, one lower case and a number).')
]