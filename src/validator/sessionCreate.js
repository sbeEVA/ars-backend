const {body} = require('express-validator');

module.exports = [
	body('name').not().isEmpty().withMessage('name required.'),
	body('open').isBoolean()
]