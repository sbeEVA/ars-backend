const {body, param} = require('express-validator');

module.exports = [
	body('name').optional().not().isEmpty().withMessage('name required.'),
	body('open').optional().isBoolean()
]