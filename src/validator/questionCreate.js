const {body} = require('express-validator');

module.exports = [
	body('text').not().isEmpty(),
	body('type').custom((value, {req}) => {
		const validTypes = ['SC', 'MC', 'S', 'RK'];
		if (validTypes.includes(value)) {
			return Promise.resolve();
		}
		return Promise.reject('type must be one of [' + validTypes.join(', ') + '].');
	}),
	body('answers').not().isEmpty(),
	body('open').isBoolean(),
	body('shareResults').isBoolean()
]