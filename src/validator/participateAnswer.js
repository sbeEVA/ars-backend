const {body} = require('express-validator');

module.exports = [
	body('answer').custom((value, {req}) => {
		if (value === undefined || value === null) {
			return Promise.reject('answer must not be empty');
		}
		if (Array.isArray(value)) {
			value.forEach((answer) => {
				if (!/^\d+$/.test(value)) {
					return Promise.reject('answer must be a number or an array of numbers.');
				}
			})
			return Promise.resolve();
		} else if (/^\d+$/.test(value)) {
			return Promise.resolve();
		}
		return Promise.reject('answer must be a number or an array of numbers.');
	})
]