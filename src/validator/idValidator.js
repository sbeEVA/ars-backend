const {param} = require('express-validator');

const validId = (value, {req}) => {
	if (!value.match(/^[0-9a-fA-F]{24}$/)) {
		return Promise.reject('id has invalid format.');
	}
	return Promise.resolve();
}

module.exports = [
	param('id').custom(validId),
	param('questionId').optional().custom(validId),
]