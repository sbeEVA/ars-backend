const mongoose = require('mongoose');
const config = require("config");

const databaseUri = process.env.MONGODB_URI || config.get('database_connection_string')
module.exports = mongoose.connect(databaseUri);